var express = require('express');
var router = express.Router();
var jsforce = require('jsforce');
var request = require('request');
var helper = require('../helpers/Utils');
require('dotenv').config();
var exdata;
router.get('/', function(req, res) {
	res.render('dashboard', { title: 'Dashboards' });
});
router.get('/uploadautodata', function(req, res) {
	if(req.query.access_token && req.query.instanceUrl && req.query.user_access_token && req.query.user_instance_url && req.query.template_id){
		var conn = new jsforce.Connection({
		  instanceUrl : req.query.instanceUrl, //req.query.instanceUrl
		  accessToken : req.query.access_token
		});
		var records = [];
		conn.query("SELECT ContentDocumentId,contentDocument.FileType,contentDocument.Title FROM ContentDocumentLink WHERE LinkedEntityId ='"+req.query.template_id+"'", function(err, fileres) {
		  if (err) { 
			console.log(err);
			res.status(400).send('No Files Found');
		  }else{
			 var resu = fileres.records;
			if(resu.length>0){
				var filelist = [];
				for(var i=0;i<resu.length;i++){
					var temp_file = {};
					if(resu[i].ContentDocument.FileType == 'CSV'){
						temp_file.type = 'CSV';
						temp_file.fileId = resu[i].ContentDocumentId;
						temp_file.Name = resu[i].ContentDocument.Title;
						temp_file.data = '';
						filelist.push(temp_file);
					}else if(resu[i].ContentDocument.FileType == 'JSON'){
						temp_file.type = 'JSON';
						temp_file.fileId = resu[i].ContentDocumentId;
						temp_file.Name = resu[i].ContentDocument.Title;
						temp_file.data = '';
						filelist.push(temp_file);
					}
					if(i == resu.length-1){
						if(filelist.length>1){
							try{
								var counter =  0;
								for(var k=0;k<filelist.length;k++){
									conn.query("SELECT ContentDocumentId,VersionData FROM ContentVersion WHERE ContentDocumentId = '"+filelist[k].fileId+"' AND IsLatest = true",function(err,resp){
										if(err){
											console.log(err);
											res.status(400).send('File Reading Failed');
										}else{
											if(resp.records.length>0){
												for(var m=0;m<filelist.length;m++){
													if(filelist[m].fileId == resp.records[0].ContentDocumentId){
													    filelist[m].VersionData = resp.records[0].VersionData;
													}
													if(m == filelist.length-1 && counter == filelist.length-1){
														helper.getfileStringfromurl(req.query.access_token,req.query.instanceUrl,filelist,function(respon){
															filelist = respon.data;
															if(respon.status == '200'){
																helper.createDataset(req.query.user_access_token,req.query.user_instance_url,filelist,function(resp){
																	if(resp.status == '200'){
																		res.status(200).send('Dataset created Succesfully');
																	}else{
																		res.status(400).send('Dataset creation failed');
																	}
																});
															}else{
																res.status(400).send('File Reading Failed');
															}
														});
													}
												}
												counter++;
											}
										}
									});
								}
							}catch(e){
								console.log(e);
							}
						}else{
							res.status(400).send('Mapping Files Missing');
						}
					}
				}
				
				//res.status(200).json(JSON.stringify(result)); 
			}else{
				res.status(400).send('Error Uploading Dataset');
			}
			
		  }
		  //console.log("total : " + result.totalSize);
		  //console.log("fetched : " + result.records.length);
		});
	}else{
		res.status(400).send('Parameters missing');
	}
});
router.post('/getpopoverdata', function(req, res) {
	if(req.body.qury && req.body.access_token && req.body.url){
		console.log('params passed');
		var obj = {};
		obj = (req.body.qury)
		var options = {
			url: req.body.url,
			method: 'POST',
			headers: {
				'content-type': 'application/json',
				'Authorization': 'Bearer '+req.body.access_token
			},
			body : obj
		};
		function respcall(error, response, body) {
			//console.log(body);
		  if (!error) {
			console.log('not error');
			//console.log(response);
			res.status(200).json(response);
		  }else{
			console.log('error');
			res.status(400).send(error);
		  }
		}
		request(options, respcall);
	}else{
		res.status(400).send('Parameters missing');
	}
});
router.post('/getotherproperties', function(req, res) {
	if(req.body.qury && req.body.access_token && req.body.url){
		//console.log('params passed');
		var obj = {};
		obj = req.body.qury;
		var options = {
			url: req.body.url,
			method: 'POST',
			headers: {
				'content-type': 'application/json',
				'Authorization': 'Bearer '+req.body.access_token
			},
			body : obj
		};
		function respcall(error, response, body) {
			//console.log(body);
		  if (!error) {
			//console.log('not error');
			//console.log(response);
			res.status(200).json(response);
		  }else{
			console.log('error');
			res.status(400).send(error);
		  }
		}
		request(options, respcall);
	}else{
		res.status(400).send('Parameters missing');
	}
});
/*router.get('/getApiversion', function(req, res) {
	if(req.query.access_token && req.query.instanceUrl && req.query.domain){
		var conn = new jsforce.Connection({
		  instanceUrl : req.query.instanceUrl,
		  accessToken : req.query.access_token
		});
		var records = [];
		conn.query("SELECT ID,API_Version__c from Heroku_App__c WHERE Domain__c='"+req.query.domain+"' LIMIT 1", function(err, result) {
		  //console.log(result);
		  if (err) { 
			console.log(err);
			res.status(400).send('Domain not Registered');
		  }
		  //console.log("total : " + result.totalSize);
		  //console.log("fetched : " + result.records.length);
		  res.status(200).json(result.records[0]);
		});
	}else{
		res.status(400).send('Parameters missing');
	}
});*/
router.post('/getDashboards', function(req, res) {
	if(req.body.access_token && req.body.instanceUrl){
		var conn = new jsforce.Connection({
		  instanceUrl : req.body.instanceUrl,
		  accessToken : req.body.access_token
		});
		var stat = req.body.filterstatus;
		//console.log(typeof stat);
		var qury = "SELECT ID,NAME,API_Version__c,No_Selections__c,Dashboard_Type__c,No_of_Datasets__c,Master_Template__c,No_of_Dates__c,No_of_Dimensions__c,No_of_Measures__c,No_of_Strings__c,Notes__c from Touch_Analytics__c WHERE ";
		for(var m=0;m<stat.length;m++){
			if(m==0){
				qury += "Dashboard_Type__c='"+stat[m]+"'";
			}else{
				qury += " OR Dashboard_Type__c='"+stat[m]+"'";
			}
		}
		var records = [];
		conn.query(qury, function(err, result) {
		  //console.log(result);
		  if (err) { 
			console.log(err);
			res.status(400).send('Access token Expired');
		  }
		  //console.log("total : " + result.totalSize);
		  //console.log("fetched : " + result.records.length);
		  res.status(200).json(result.records);
		});
	}else{
		res.status(400).send('Access token missing');
	}
});
router.post('/Savecopy', function(req, res) {
	//console.log(req.body);
	if(req.body.access_token && req.body.instance_url){
		var conn = new jsforce.Connection({
		  instanceUrl : req.body.instance_url,
		  accessToken : req.body.access_token
		});
		var obj = {};
		obj.Selected_Values__c = req.body.dsdata;
		obj.Contact__c = req.body.contact;
		obj.Template_Used__c = req.body.template;
		obj.Name = req.body.dashboardname;
		conn.sobject("Dashboards__c").create(obj, function(err, ret) {
			if (err || !ret.success) { 
				console.log(err);
				return res.status(400).send(err);
			}else{
				return res.status(200).send(ret.id);
			}
		});
	}else{
		res.status(400).send('Failed receiving parameters');
	}
});
router.post('/connecturl', function(req, res) {
	//console.log(req.body);
	if(req.body.access_token && req.body.url){
		if(req.body.type && req.body.type == 'nversion'){
			var options = {
				url: req.body.url,
				headers: {
					'Authorization': 'OAuth '+req.body.access_token
				}
			};
			function respcall(error, response, body) {
				var init_resp = JSON.parse(response.body);
				if(init_resp.versions.length>0){
				  if (!error && response.statusCode == 200) {
					var options12 = {
						url: req.body.domain_url+init_resp.versions[0].url+'/xmds/main',
						headers: {
							'Authorization': 'OAuth '+req.body.access_token
						}
					};
					function respcall12(error, response, body) {
					  if (!error && response.statusCode == 200) {
						res.status(200).json(body);
					  }else{
						res.status(400).send(error);
					  }
					}
					request(options12, respcall12);
				  }else{
					res.status(400).send(error);
				  }
				}else{
					res.status(400).send('No version found');
				}
			}
			request(options, respcall);
		}else{
			var options = {
				url: req.body.url,
				headers: {
					'Authorization': 'OAuth '+req.body.access_token
				}
			};
			function respcall(error, response, body) {
			  if (!error && response.statusCode == 200) {
				res.status(200).json(body);
			  }else{
				console.log("err is"+error);
				res.status(400).send(error);
			  }
			}
			request(options, respcall);
		}
	}else{
		res.status(400).send('Failed receiving parameters');
	}
});
router.post('/createdataset', function(req, res) {
	
});
router.get('/getUserprofile', function(req, res) {
	if(req.query.cust_access_token && req.query.profileUrl){
		var options = {
			url: req.query.profileUrl,
			headers: {
				'Authorization': 'OAuth '+req.query.cust_access_token
			}
		};
		function respcall(error, response, body) {
			console.log(error);
			console.log(response);
		  if (!error && response.statusCode == 200) {
			res.status(200).json(body);
		  }else{
			res.status(400).send(error);
		  }
		}
		request(options, respcall);
	}
});
router.get('/getOrgName', function(req, res) {
	if(req.query.cust_access_token && req.query.orgUrl){
		var options = {
			url: req.query.orgUrl,
			headers: {
				'Authorization': 'OAuth  '+req.query.cust_access_token
			}
		};
		function respcall(error, response, body) {
		  if (!error && response.statusCode == 200) {
			res.status(200).json(body);
		  }else{
			console.log(error);
			console.log(response);
			console.log(body)
			res.status(400).send(error+''+response+''+body);
		  }
		}
		request(options, respcall);
	}
});
router.get('/getDashboarddata', function(req, res) {
	if(req.query.access_token && req.query.instanceUrl && req.query.dbid){
		var conn = new jsforce.Connection({
		  instanceUrl : req.query.instanceUrl,
		  accessToken : req.query.access_token
		});
		var records = [];
		// Dataset_Card_Util__c has been stripped from the query because it caused it to fail
		conn.query("SELECT ID,Name,No_of_Strings__c,No_of_Dimensions__c,No_of_Dates__c,No_of_Measures__c,No_of_Datasets__c,Connect_dashboard_String1__c,Connect_dashboard_String2__c,Connect_dashboard_String3__c,Connect_dashboard_String4__c,Connect_dashboard_String5__c,API_Version__c from Touch_Analytics__c WHERE Id='"+req.query.dbid+"'", function(err, result) {
		  //console.log(result);
		  if (err) { 
			console.log(err);
			res.status(400).send('Access token Expired');
		  }
		  //console.log("total : " + result.totalSize);
		  //console.log("fetched : " + result.records.length);
		  res.status(200).json(result.records);
		  
		});
	}else{
		res.status(400).send('Access token missing');
	}
});
router.get('/getChildDashboards', function(req, res) {
	if(req.query.access_token && req.query.instanceUrl && req.query.dbid){
		var conn = new jsforce.Connection({
		  instanceUrl : req.query.instanceUrl,
		  accessToken : req.query.access_token
		});
		var records = [];
		conn.query("SELECT ID,Name,No_of_Strings__c,No_of_Dimensions__c,No_of_Dates__c,No_of_Measures__c,No_of_Datasets__c,Connect_dashboard_String1__c,Connect_dashboard_String2__c,Connect_dashboard_String3__c,Connect_dashboard_String4__c,Connect_dashboard_String5__c from Touch_Analytics__c WHERE Master_Template__c='"+req.query.dbid+"'", function(err, result) {
		  //console.log(result);
		  if (err) { 
			console.log(err);
			res.status(400).send('Access token Expired');
		  }
		  //console.log("total : " + result.totalSize);
		  //console.log("fetched : " + result.records.length);
		  res.status(200).json(result.records);
		  
		});
	}else{
		res.status(400).send('Access token missing');
	}
});
router.get('/getPreviousDashboards', function(req, res) {
	if(req.query.access_token && req.query.instanceUrl && req.query.conid && req.query.temid ){
			console.log('reached');
		//console.log("SELECT ID,Name,Selected_Values__c,CreatedDate  FROM Dashboard__c WHERE Contact__c='"+req.query.conid+"' AND Template_Used__c='"+temid+"'");
		var conn = new jsforce.Connection({
		  instanceUrl : req.query.instanceUrl,
		  accessToken : req.query.access_token
		});
		console.log(conn);
		var records = [];
		conn.query("SELECT ID,Name,Selected_Values__c,CreatedDate FROM Dashboards__c WHERE Contact__c='"+req.query.conid+"' AND Template_Used__c = '"+req.query.temid+"'", function(err, result) {
		  console.log(result)
		  if (err) { 
			console.log(err);
			res.status(400).send('Access token Expired');
		  }
		  //console.log("total : " + result.totalSize);
		  //console.log("fetched : " + result.records.length);
		  res.status(200).json(result.records);
		  
		});
	}else{
		res.status(400).send('Access token missing');
	}
});
router.post('/createdashboard', function(req, res) {
	if(req.body.data && req.body.access_token && req.body.url){
		var options = {
			url: req.body.url,
			method: 'POST',
			headers: {
				'content-type': 'application/json',
				'Authorization': 'Bearer '+req.body.access_token
			},
			body :req.body.data
		};
		function respcall(error, response, body) {
		  if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
			res.status(200).json(response);
		  }else{
			res.status(400).send(response);
		  }
		}
		request(options, respcall);
	}else{
		res.status(400).send('Parameters missing');
	}
});
router.post('/createdb', function(req, res) {
	if(req.body.template_id && req.body.org_access_token && req.body.org_url){
		var conn = new jsforce.Connection({
		  instanceUrl : req.body.org_url,
		  accessToken : req.body.org_access_token
		});
		var records = [];
		conn.query("SELECT ID,Name,No_of_Strings__c,No_of_Dimensions__c,No_of_Dates__c,No_of_Measures__c,No_of_Datasets__c,Connect_dashboard_String1__c,Connect_dashboard_String2__c,Connect_dashboard_String3__c,Connect_dashboard_String4__c,Connect_dashboard_String5__c,API_Version__c from Touch_Analytics__c WHERE Id='"+req.body.template_id+"'", function(err, result) {
		  console.log(result);
		  if (err) { 
			console.log(err);
			res.status(400).send('Access token Expired 440');
		  }else{
			//console.log(result.records);
						console.log(result.records.length);
			if(result.records.length >0){
				var final_str = '';
				var listofarrays = ['Connect_dashboard_String1__c','Connect_dashboard_String2__c','Connect_dashboard_String3__c','Connect_dashboard_String4__c','Connect_dashboard_String5__c'];
				var crnt_rec = result.records[0];
				console.log('-----------------------------------------------------------------------------------------');
				console.log(crnt_rec);
				for(var i=0;i<listofarrays.length;i++){
					if(crnt_rec[listofarrays[i]]){
						final_str += crnt_rec[listofarrays[i]];
					}
					if(i == listofarrays.length-1){
						final_str = final_str.replace(/#NewFolderId#/g,req.body.user_id);
						var options = {
							url: req.body.customerurl,
							method: 'POST',
							headers: {
								'content-type': 'application/json',
								'Authorization': 'Bearer '+req.body.cust_access_token
							},
							body : final_str
						};
						request(options, function(error, response, body){
						  if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
							res.status(200).json(response);
						  }else{
							console.log(error);
							res.status(400).send(response);
						  }
						});
					}
				}
			 }else{
				res.status(400).send('Dashboard Creation Failed');
			 }
		   }
		});	
	}else{
		res.status(400).send('Parameters missing');
	}
});

module.exports = router;
