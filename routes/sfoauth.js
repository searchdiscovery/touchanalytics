	var express = require('express');
	var router = express.Router();
	var jsforce = require('jsforce');
	var url  = require('url');
	require('dotenv').config()
	
	var oauth2 = new jsforce.OAuth2({
	  loginUrl : 'https://login.salesforce.com',
	  clientId : process.env.PARTNER_CLIENT_ID,
	  clientSecret : process.env.PARTNER_CLIENT_SECRET,
	  redirectUri : process.env.REDIRECT_URL
	});
	router.get('/login', function(req, res) {
		//console.log(req.query);
		if(req.query.ins == 'prod'){
			var red_url = "https://login.salesforce.com/services/oauth2/authorize?response_type=token&prompt=login%20consent&display=popup&client_id="+process.env.PARTNER_CLIENT_ID+"&redirect_uri="+process.env.REDIRECT_URL;
			res.redirect(red_url);
		}else{
			var red_url = "https://test.salesforce.com/services/oauth2/authorize?response_type=token&prompt=login%20consent&display=popup&client_id="+process.env.PARTNER_CLIENT_ID+"&redirect_uri="+process.env.REDIRECT_URL;
			res.redirect(red_url);
		}
	});
	router.get('/partnerlogin', function(req, res) {
	  var conn = new jsforce.Connection({ oauth2 : oauth2 });
		conn.login(process.env.SF_USER_NAME, process.env.SF_PASSWORD, function(err, userInfo) {
			let obj = {};
			if(conn.accessToken && conn.instanceUrl){
				obj.stat = true;
				obj.access_token = conn.accessToken;
				obj.instanceUrl = conn.instanceUrl;
				res.json(obj);
				//var red_url = "/callback?access_token="+conn.accessToken+"&instance_url="+conn.instanceUrl;
				//res.redirect(red_url);
			}else{
				obj.stat = false;
				res.json(obj);
			}
		});
	});
	router.get('/callback', function(req, res) {
		res.render('transit', { title: 'Transit Page' });
		/*var code = req.param('code');
		conn.authorize(code, function(err, userInfo) {
		if (err) { return res.status(400).send(err); }
		// Now you can get the access token, refresh token, and instance URL information.
		// Save them to establish connection next time.
		console.log(conn);
		console.log(conn.accessToken);
		console.log(conn.refreshToken);
		console.log(conn.instanceUrl);
		console.log("User ID: " + userInfo.id);
		console.log("Org ID: " + userInfo.organizationId);
		if(conn.accessToken){
			var red_url = "/dashboard?access_token="+conn.accessToken+"&instanceUrl="+conn.instanceUrl;
			res.redirect(red_url);
		}
		});*/
	});
module.exports = router;
