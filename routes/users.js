var express = require('express');
var router = express.Router();
var jsforce = require('jsforce');
var helper = require('../helpers/Utils');

/* GET users listing. */
router.post('/logout', function(req, res, next) {
	if(req.body.access_token){
		var conne = new jsforce.Connection({
		  sessionId : req.body.access_token,
		  serverUrl : req.body.instanceUrl
		});
		conne.logout(function(err) {
		  if (err) { console.log(err); 
		  return res.status(400).send(err);
		  }
			return res.status(200).send('Signed off');
		});
	}else{
		return res.status(400).send('parameters missing');
	}

});
router.post('/createuser', function(req, res, next) {
	if(req.body.access_token && req.body.instance_url){
		var con = new jsforce.Connection({
		  instanceUrl : req.body.instance_url,
		  accessToken : req.body.access_token
		});
		var accId;
		var conId;
		con.query("SELECT ID,NAME from Account WHERE NAME ='"+req.body.data.organization_name+"' LIMIT 1", function(err, result) {
		    console.log(result);
			if (err) { 
				console.log(err);
				res.status(400).send(err);
			}
			if(result.records.length>0){
				accId = result.records[0].Id;
				helper.createContact(con,accId,req,function(resp){
					if(resp.status){
						return res.status(200).json(resp.data);
					}else{
						return res.status(400).send(resp.error);
					}
				});
			}else{
                con.sobject("Account").create({ Name: req.body.data.organization_name, RecordTypeId: '0122M000000viuSQAQ' }, function(err, ret) {
					if (err || !ret.success) { 
						return res.status(400).send(err);
					}else{
						console.log('acc'+ret.id);
						accId = ret.id;
						helper.createContact(con,accId,req,function(resp){
							if(resp.status){
								return res.status(200).json(resp.data);
							}else{
								return res.status(400).send(resp.error);
							}
						});
					}
				});
			}
		});
	}else{
		return res.status(400).send('parameters missing');
	}

});

module.exports = router;
