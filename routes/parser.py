import collections
import csv
import json
from http.cookiejar import CookieJar
import pandas as pd
from flask import (Flask, Response, redirect, render_template,
                   render_template_string, request, url_for)

app = Flask(__name__)



def load_json_from_file(filename, object_pairs_hook=collections.OrderedDict):
    fl = open(filename, "r")
#     fl_json = json.load(fl, object_pairs_hook=object_pairs_hook)
    fl_json = json.load(fl, object_pairs_hook=object_pairs_hook)
    fl.close()
    return fl_json

def dump_json_to_file(filename,json_obj,sort_keys=False):
    fl = open(filename, "w")
    json.dump(json_obj, fl, indent=2)
    fl.close()

def extract_dataflow(df_fl_json,mini_dataflow, dfNode):
#     pprint.pprint(df_fl_json.keys())
    if dfNode not in df_fl_json:
        # print("DATAFLOW: ",DATAFLOW_FILE, " doesn't have node :", dfNode)
        return [dfNode]
    node = df_fl_json[dfNode]
    mini_dataflow[dfNode] = df_fl_json[dfNode]
    
    if node["action"] in ["sfdcRegister","computeExpression","computeRelative","flatten","sliceDataset","filter"]:
        extract_dataflow(df_fl_json,  mini_dataflow, node["parameters"]["source"])
    elif node["action"] == "augment":
        extract_dataflow(df_fl_json,  mini_dataflow, node["parameters"]["left"])
        if node["parameters"]["right"] not in mini_dataflow:
            extract_dataflow(df_fl_json,  mini_dataflow, node["parameters"]["right"])
    elif node["action"] in ["sfdcRegister","sfdcDigest","edgemart"]:
        return mini_dataflow
    elif node["action"] in ["append"]:
        for n in node["parameters"]["sources"]:
            extract_dataflow(df_fl_json,  mini_dataflow, n)
    else:
        print("NOT HANDLING ACTION ", node["action"])
#         pprint.pprint(node)
    return mini_dataflow
def trace_dataflow(node, dataflow):
    focus = dataflow[node]
    if focus['action'] in ['sfdcDigest', 'digest', 'edgemart']:
        return focus['parameters']['object'] if focus['action'] == 'sfdcDigest' else focus['parameters']['alias'] 
    else:
        nextNode = focus['parameters']['left'] if focus['action'] == 'augment' else focus['parameters']['source']
        return trace_dataflow(nextNode, dataflow)

@app.route('/', methods=['POST', 'GET'])
def hello():
    # focusNode = request.args.get('currentNode')
    if request.method=='GET':
        return render_template('/parser.ejs')
    elif request.method=='POST': 
        fileFocus = request.files['file'].read()
        fileFocus = json.loads(fileFocus)
        nodeList = list(fileFocus)
        DATASET_NODE = request.form.get('node')
        mini_dataflow = {}
        node_count = {}
        register_nodes =[]
        for k in fileFocus.keys():
            if fileFocus[k]["action"] == "sfdcRegister":
                register_nodes.append(k)
            if k in node_count:
                node_count[k] = node_count[k] + 1
            else:  
                node_count[k] = 1
            cnt = 0
            j = None
            if "source" in fileFocus[k]["parameters"]:
                j = fileFocus[k]["parameters"]["source"]
                if "j" in node_count:
                    node_count[j] = node_count[j] + 1
                else:  
                    node_count[j] = 1
            if "left" in fileFocus[k]["parameters"]:
                j = fileFocus[k]["parameters"]["left"]
                if "j" in node_count:
                    node_count[j] = node_count[j] + 1
                else:  
                    node_count[j] = 1
            if "right" in fileFocus[k]["parameters"]:
                j = fileFocus[k]["parameters"]["right"]
                if "j" in node_count:
                    node_count[j] = node_count[j] + 1
                else:  
                    node_count[j] = 1
            if "sources" in fileFocus[k]["parameters"]:
                for src in fileFocus[k]["parameters"]["sources"]:
                    j = src
                    if "j" in node_count:
                        node_count[j] = node_count[j] + 1
                    else:  
                        node_count[j] = 1
        fin = extract_dataflow(fileFocus, mini_dataflow,DATASET_NODE)     
        return Response(json.dumps(fin), 
            mimetype='application/json',
            headers={'Content-Disposition':'attachment;filename=dataflow.json'})
