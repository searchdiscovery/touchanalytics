var express = require('express');
var router = express.Router();
var jsforce = require('jsforce');
var conn = new jsforce.Connection({
  loginUrl : 'https://login.salesforce.com'
});
/* GET users listing. */
router.post('/authenicatecust', function(req, res, next) {
	//console.log(req.body);
	if(req.body.username){
		conn.login(req.body.username, req.body.password, function(err, userInfo) {
			console.log(err);
			if (err) { return res.status(400).send(err); }
			if(conn.accessToken){
			  var send_obj = {};
			  send_obj.cus_access_token = conn.accessToken;
			  send_obj.instanceUrl = conn.instanceUrl;
			  return res.status(200).json(JSON.stringify(send_obj));
			}else{
				console.log(err);
				return res.status(400).send('Accesstoken error');
			}
		});
	}else{
		return res.status(400).send('parameters missing');
	}

});


module.exports = router;
