	var request = require('request');
	
	function reqmethod(org_access_token, org_instance_url,filelist,n,cb){
		var req_options = {
				url: org_instance_url+''+filelist[n].VersionData,
				headers: {
					'Content-Type' : 'application/json',
					'Authorization': 'OAuth '+org_access_token
				}
			};
		function req_func(error, response, body){
			if (!error && response.statusCode == 200) {
				filelist[n].filedata = Buffer.from(body).toString('base64');
				cb(filelist);
			}else{
				console.log(error);
			}
		}
		request(req_options, req_func);
	}
	function reqskeleton(access_token,instance_url,addurl,tem_ob,cb){
		console.log(instance_url+''+addurl);
		console.log(tem_ob);
		var options = {
			url: instance_url+''+addurl,
			method: 'POST',
			headers: {
				'content-type': 'application/json',
				'Authorization': 'OAuth '+access_token
			},
			body : JSON.stringify(tem_ob)
		};
		request(options, function(error, response, body){
			console.log('in parent response');
			console.log(error);
			console.log(body);
		    //console.log(response);
			console.log(response.statusCode);
			if (!error && (response.statusCode == 200 || response.statusCode == 201)) {
				console.log('in cb');
			  	cb(body);
			}
		});
	}
	function getfileStringfromurl(org_access_token, org_instance_url,filelist,cb){
		var countr = 0;
		var resp_ob = {};
		for(var n=0;n<filelist.length;n++){
			reqmethod(org_access_token, org_instance_url,filelist,n,function(newfilelist){
				console.log(newfilelist);
				if(countr == filelist.length-1){
					resp_ob.status = '200';
					resp_ob.data = newfilelist;
					cb(resp_ob);
				}
				countr++;
			});
			
		}
	}
	function CreateContact(con,accId,req,cb) {
		var respobj= {}
		if(accId){
			con.query("SELECT ID,AccountId from Contact WHERE Username__c ='"+req.body.data.username+"' LIMIT 1", function(err, result) {
				console.log('3');
				console.log(err);
				console.log(result);
				if (err){ 
					console.log(err);
					//return res.status(400).send(err);
					respobj.status = false;
					respobj.error = err;
					cb(respobj);
				}else{
					console.log(result.records);
					if(result.records.length>0){
						conId = result.records[0].Id;
						if(conId){
							con.query("SELECT Id,NAME,Dashboard_Type_Access__c,Card_Util__c from Contact WHERE Id ='"+conId+"'", function(err, result) {	
								if (err) { 
									//return res.status(400).send(err);
									respobj.status = false;
									respobj.error = err;
									cb(respobj);
								}else{
									console.log('5');
									console.log(result.records[0]);
									respobj.status = true;
									respobj.error = '';
									respobj.data = result.records[0];
									cb(respobj);
									//res.status(200).json(result.records[0]);
								}
							});
						}
					}else{
						if(req.body.data.last_name == '' || !req.body.data.last_name){
							req.body.data.last_name = 'User'
						}
						var new_con = {
							FirstName: req.body.data.first_name,
							LastName: req.body.data.last_name,
							AccountId:accId,
							Email: req.body.data.email,
							Dashboard_Type_Access__c: 'Free',
                            Username__c: req.body.data.username,
                            RecordTypeId: '0122M000000viuTQAQ'

						}
						con.sobject("Contact").create(new_con, function(err, ret) {
							if (err || !ret.success) { 
								console.log(err);
								respobj.status = false;
								respobj.error = err;
								cb(respobj);
								//return res.status(400).send(err);
							}else{
								conId = ret.id;
								if(conId){
									con.query("SELECT Id,NAME,Dashboard_Type_Access__c,Card_Util__c from Contact WHERE Id ='"+conId+"'", function(err, result) {	
										if (err) { 
											respobj.status = false;
											respobj.error = err;
											cb(respobj);
											//return res.status(400).send(err);
										}else{
											respobj.status = true;
											respobj.error = '';
											respobj.data = result.records[0];
											cb(respobj);
										}
									});
								}
							}
						});
					}
				}
			});
		}
	};
	function createDataset(access_token,instance_url,filelist,cb){
		if(access_token && instance_url){
			console.log(filelist);
		var return_res= {};
		var addurl;
			try{
				for(var i=0;i<filelist.length;i++){
					if(filelist[i].type == 'JSON'){
						var json_data = filelist[i];
					}else if(filelist[i].type == 'CSV'){
						var csv_data = filelist[i];
					}
					if(i == filelist.length-1 && json_data && csv_data){
						var tem_ob = {};
						tem_ob.Format = 'csv';
						tem_ob.EdgemartAlias = json_data.Name;
						tem_ob.Operation = 'Overwrite';
						tem_ob.Action = 'None';
						tem_ob.MetadataJson = json_data.filedata;
						addurl = '/services/data/v47.0/sobjects/InsightsExternalData';
						reqskeleton(access_token,instance_url,addurl,tem_ob,function(init_res){
							console.log(init_res);
							console.log(typeof init_res);
							init_res = JSON.parse(init_res);
							console.log(typeof init_res);
							console.log('1 resp');
							if(init_res.id){
								var file_ob = {};
								file_ob.InsightsExternalDataId = init_res.id;
								file_ob.PartNumber = '1';
								file_ob.DataFile = csv_data.filedata;
								addurl = '/services/data/v47.0/sobjects/InsightsExternalDataPart';
								reqskeleton(access_token,instance_url,addurl,file_ob,function(fileresp){
									console.log('2 resp');
									console.log(fileresp);
									var sec_res = {};
									sec_res.Action = "Process";
									var options3 = {
										url: instance_url+'/services/data/v47.0/sobjects/InsightsExternalData/'+init_res.id,
										method: 'PATCH',
										headers: {
											'content-type': 'application/json',
											'Authorization': 'Bearer '+access_token
										},
										body : JSON.stringify(sec_res)
									};
									function respcall3(err, respons, rbody) {
										console.log('last callback');
										console.log(err);
										var temp_j = {};
										if(!err){
											temp_j.status = '200';
											cb(temp_j);
										}else{
											temp_j.status = '400';
											cb(temp_j)
										}
										//console.log(respons);
										console.log(rbody);
									}
									request(options3, respcall3);
								});
							}
						});							
					}
				}
			}catch(e){
				console.log("entering catch block");
				console.log(e);
				console.log("leaving catch block");
			}
		}else{
			return_res.status = '400';
			cb(return_res);
		}
	}
	
module.exports = {
  createContact: CreateContact,
  createDataset: createDataset,
  getfileStringfromurl: getfileStringfromurl
};