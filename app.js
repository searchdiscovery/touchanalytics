var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var customersRouter = require('./routes/customers');
var sfRouter = require('./routes/sfoauth');
var dashboardRouter = require('./routes/dashboard');
var dataflowRouter = require('./routes/dataflow');
// var dataflowParser = require('./routes/parser.py');
var app = express();
const port = process.env.PORT

//app.use(express.limit('5mb'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser({limit: '5mb'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(express.json());
//app.use(express.limit('5mb'));
//app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/customers', customersRouter);
app.use('/oauth2', sfRouter);
app.use('/dashboard', dashboardRouter);
app.use('/dataflow', dataflowRouter);
// app.use('/parser', dataflowParser);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
if(process.env.ENVRIONMENT == 'DEV') {
  app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
}
module.exports = app;
