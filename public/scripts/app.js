	window.replaceAll = function (str, find, replace) {
         return str.replace(new RegExp(find, 'g'), replace);
    }
	window.onbeforeunload = function(e) {
	  var dialogText = 'Are you sure you want to close? You will loose all your changes.';
	  e.returnValue = dialogText;
	  return dialogText;
	};
     window.send_obj = {
         access_token: '',
         url: '',
         data: ''
     }
     window.attachev = function () {
         $('#analyticsdim').on('mouseenter', 'option', function (e) {
             console.log('cccccc');
             //alert(this.value, 'Yeah');
             e.preventDefault();
             e.stopPropagation();
             // this refers to the option so you can do this.value if you need..
         });
     }
     window.recheck = function () {
         if (typeof (Event) === 'function') {
             // modern browsers
             window.dispatchEvent(new Event('resize'));
         } else {
             // for IE and other old browsers
             // causes deprecation warning on modern browsers
             var evt = window.document.createEvent('UIEvents');
             evt.initUIEvent('resize', true, false, window, 0);
             window.dispatchEvent(evt);
         }
     }
     window.getParams = function (url) {
         var params = {};
         var parser = document.createElement('a');
         parser.href = url;
         var query = parser.search.substring(1);
         var vars = query.split('&');
         for (var i = 0; i < vars.length; i++) {
             var pair = vars[i].split('=');
             params[pair[0]] = decodeURIComponent(pair[1]);
         }
         return params;
     };
     var TouchApp = angular.module('analyticsapp', ['ngSanitize', 'ui.sortable', 'ui.grid', 'ui.grid.selection', 'ui.grid.pagination', 'ui.grid.autoResize', 'ui.bootstrap','ui.select']);
     TouchApp.directive("selectNgFiles", function() {
		  return {
			require: "ngModel",
			link: function postLink(scope,elem,attrs,ngModel) {
			  elem.on("change", function(e) {
				var files = elem[0].files;
				console.log(files);
				ngModel.$setViewValue(files);
			  })
			}
		  }
		});
	 TouchApp.filter('propsFilter', function() {
		  return function(items, props) {
			var out = [];

			if (angular.isArray(items)) {
			  var keys = Object.keys(props);

			  items.forEach(function(item) {
				var itemMatches = false;

				for (var i = 0; i < keys.length; i++) {
				  var prop = keys[i];
				  var text = props[prop].toLowerCase();
				  if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
					itemMatches = true;
					break;
				  }
				}

				if (itemMatches) {
				  out.push(item);
				}
			  });
			} else {
			  // Let the output be the input untouched
			  out = items;
			}

			return out;
		  };
		});
	 
	 TouchApp.controller('analyticsctrl', ['$scope', '$location', '$timeout', '$window', '$rootScope', '$filter', '$compile', '$sce', '$http', function ($scope, $location, $timeout, $window, $rootScope, $filter, $compile, $sce, $http) {
         $scope.selected = {
             dashboard: '',
             folder: '',
             dashoption: '',
             dataset: [],
			 datasetfile:'',
			 datasetconfig: ''
         }
		 $scope.datasetSearch = '';
         $scope.gohome = function () {
             $scope.metricsec = false;
             $scope.datasetsec = false;
             $scope.dashboardsec = true;
			 window.location.reload();
         }
		 $scope.showotherprops = false;
         $scope.selected_dashboard;
         $scope.gridOptions = {
             enableSorting: true,
             enableFiltering: true,
             enableFullRowSelection: true,
             multiSelect: false,
             paginationPageSize: 10,
             columnDefs: [
                 { field: 'Name', displayName: 'Template Name' },
                 // { field: 'Dashboard_Type__c', displayName: 'Dashboard Type' },
                 { field: 'No_of_Datasets__c', displayName: '# of Datasets' },
                 { field: 'No_of_Dates__c', displayName: '# of Dates' },
                 { field: 'No_of_Dimensions__c', displayName: '# of Dimensions' },
                 { field: 'No_of_Measures__c', displayName: '# of Measures' },
                 { field: 'No_of_Strings__c', displayName: '# of Strings' }
             ],
             onRegisterApi: function (gridApi) {
                 $scope.gridApi1 = gridApi;
                 gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                     $scope.selected_dashboard = row.entity.Id;
                     $scope.updatehelptext();
                 });
                 $timeout(function () {
                     $scope.gridApi1.core.handleWindowResize();
                 });
             }
         };
         $scope.gridOptions2 = {
             enableSorting: true,
             enableFiltering: true,
             enableFullRowSelection: true,
             multiSelect: false,
             paginationPageSize: 10,
             columnDefs: [
                 { field: 'Name', displayName: 'Name' },
                 { field: 'CreatedDate', displayName: 'Created Date', type: 'date', cellFilter: 'date:\'yyyy-MM-dd h:mm:ss\'' }
             ],
             onRegisterApi: function (gridApi) {
                 $scope.gridApi2 = gridApi;
             }
         };
		 $scope.getNumber = function(num) {
			return new Array(num);   
		}
         $scope.updatedmorder = function (ind) {
             if ($scope.datasetdata[ind].dim_direction == true) {
                 $scope.datasetdata[ind].dim_direction = false;
             } else {
                 $scope.datasetdata[ind].dim_direction = true;
             }
         }
         $scope.updatepoch = function (ind, type) {
             if ($scope.datasetdata[ind]) {
                 if (type == 'dimension') {
                     if ($scope.datasetdata[ind].removedimepoch) {
                         var remove_index = [];
                         for (var d = 0; d < $scope.datasetdata[ind].dimensions.length; d++) {
                             if ($scope.datasetdata[ind].dimensions[d].field.indexOf('Date_') !== -1 || $scope.datasetdata[ind].dimensions[d].field.indexOf('date_') !== -1) {
                                 $scope.datasetdata[ind].dimensions.splice(d, 1);
                                 d--;
                             }
                         }
                     } else {
                         $scope.datasetdata[ind].dimensions = angular.copy($scope.datasetdata[ind].const_dimensions);
                     }
                 } else if (type == 'measure') {
                     console.log($scope.datasetdata[ind].removemesepoch);
                     if ($scope.datasetdata[ind].removemesepoch) {
                         for (var m = 0; m < $scope.datasetdata[ind].measures.length; m++) {
                             if ($scope.datasetdata[ind].measures[m].field.indexOf('_epoch') !== -1 || $scope.datasetdata[ind].measures[m].field.indexOf('_EPOCH') !== -1) {
                                 $scope.datasetdata[ind].measures.splice(m, 1);
                                 m--;
                             }
                         }
                         for (var q = 0; q < $scope.datasetdata[ind].uniquefields.length; q++) {
                             if ($scope.datasetdata[ind].uniquefields[q].field.indexOf('_epoch') !== -1 || $scope.datasetdata[ind].uniquefields[q].field.indexOf('_EPOCH') !== -1 || $scope.datasetdata[ind].uniquefields[q].field.indexOf('Date_') !== -1 || $scope.datasetdata[ind].uniquefields[q].field.indexOf('date_') !== -1) {
                                 $scope.datasetdata[ind].uniquefields.splice(q, 1);
                                 q--;
                             }
                         }
                     } else {
                         $scope.datasetdata[ind].measures = angular.copy($scope.datasetdata[ind].const_measures);
                         $scope.datasetdata[ind].uniquefields = angular.copy($scope.datasetdata[ind].const_uniquefields);
                     }
                 }
             }
         }
         var self = this;
         $scope.db_helptext = '';
         $scope.user_profile;
         $scope.measureid = [];
         $scope.saveobj = {};
         $scope.default_api_version = ''
         $scope.newdashboardname = '';
         $scope.querydimval = function (dataset_id, versionId, field) {
             if (localStorage.getItem('cust_instanceUrl')) {
                 var fet_url = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/query';
                 var query = JSON.stringify({ "query": "q = load \"" + dataset_id + "/" + versionId + "\"; q = group q by '" + field + "'; q = foreach q generate '" + field + "' as '" + field + "', count() as 'count'; q = order q by 'count' desc; q = limit q 5;" });
                 var s_obj = {};
                 s_obj.access_token = localStorage.getItem('cust_access_token');
                 s_obj.url = fet_url;
                 s_obj.qury = query;
                 $http({
                     url: '/dashboard/getpopoverdata',
                     method: "POST",
                     data: JSON.stringify(s_obj)
                 }).then(function successCallback(resp) {
                     $scope.popoverdata = {};
                     $scope.popoverdata.title = field;
                     $scope.popoverdata.data = [];
                     console.log(resp);
                     resp = JSON.parse(resp.data.body);
                     if (resp.results && resp.results.records && resp.results.records.length > 0) {
                         for (var p = 0; p < resp.results.records.length; p++) {
                             if (resp.results.records[p][field]) {
                                 $scope.popoverdata.data.push(resp.results.records[p][field] + ' ('+ resp.results.records[p].count + ')');
                             }
                         }
                     }
                 }, function errorCallback(response) {
                     $scope.popoverdata = {};
                     //window.location.href = "https://" + window.location.hostname;
                 });
             }
         }
         $scope.updatehelptext = function () {
             if ($scope.selected_dashboard) {
                 $scope.current_dashboardname = $scope.get_dashboard_name($scope.selected_dashboard, 'Name')
                 $scope.db_helptext = $scope.get_dashboard_name($scope.selected_dashboard, 'Notes__c');
             }
         }
         $scope.showmodal = function () {
             console.log($scope.db_helptext)
             if ($scope.selected_dashboard && $scope.db_helptext != null && $scope.db_helptext != '') {
                 $('#helpModal').modal({
                     show: true
                 });
             }
         }
         $scope.validatedata = function (ind, identity) {
             if (identity == 'dimension') {
                 if ($scope.datasetdata[ind].selecteddim.length > $scope.dashboarddata.No_of_Dimensions__c) {
                     console.log($scope.datasetdata[ind].selecteddim);
                     $timeout(function () {
                         $scope.datasetdata[ind].selecteddim.pop();
                         $.notify("Exceeded Maximum Dimensions to be selected", "warn");
                         console.log($scope.datasetdata[ind].selecteddim);
                     }, 500);
                 }
             } else if (identity == 'date') {
                 if ($scope.datasetdata[ind].selecteddate.length > $scope.dashboarddata.No_of_Dates__c) {
                     console.log($scope.datasetdata[ind].selecteddate);
                     $timeout(function () {
                         $scope.datasetdata[ind].selecteddate.pop();
                         $.notify("Exceeded Maximum Dates to be selected", "warn");
                         console.log($scope.datasetdata[ind].selecteddate);
                     }, 500);
                 }
             }
         }
         $scope.getorgname = function (orgid, cb) {
             var org_url = localStorage.getItem('cust_instanceUrl') + '/services/data/v42.0/sobjects/Organization/' + orgid + '?fields=Name';
             $http({
                 url: '/dashboard/getOrgName?cust_access_token=' + localStorage.getItem('cust_access_token') + '&orgUrl=' + org_url,
                 method: "GET"
             }).then(function successCallback(orgdetails) {
                 let resp = JSON.parse(orgdetails.data);
                 console.log(resp);
                 cb(resp.Name);
             }, function errorCallback(response) {
                 console.log(response);
                 //window.location.href = "https://"+window.location.hostname;
             });
         }
         $scope.getuserprofile = function () {
             $http({
                 url: '/dashboard/getUserprofile?cust_access_token=' + localStorage.getItem('cust_access_token') + '&profileUrl=' + localStorage.getItem('cust_profile'),
                 method: "GET"
             }).then(function successCallback(user_profile) {
                 $scope.user_profile = JSON.parse(user_profile.data);
                 if ($scope.user_profile.organization_id && $scope.user_profile.email) {
                     /*$scope.getorgname($scope.user_profile.organization_id, function (resp) {
                     });*/
					 var n = $scope.user_profile.email.lastIndexOf("@")+1;
					 var fn = $scope.user_profile.email.lastIndexOf(".");
					 var domain =  $scope.user_profile.email.substring(n,fn);
					 console.log(domain);
					$scope.user_profile.organization_name = domain;
					$scope.create_user();
                 } else {
                     window.location.href = "https://" + window.location.hostname;
                 }
             }, function errorCallback(response) {
                 console.log(response);
                 window.location.href = "https://" + window.location.hostname;
             });
         }
         $scope.getdashdata = function (status) {
             $http({
                 url: '/dashboard/getDashboards',
                 method: "POST",
                 data: JSON.stringify({ access_token: localStorage.getItem('access_token'), instanceUrl: localStorage.getItem('instanceUrl'), filterstatus: status })
             }).then(function successCallback(dash_data) {
                 $scope.loading = false;
                 $.notify("Connected to partner Org", "success");
                 $scope.dashboards = dash_data.data;
                 //$scope.dashboardTable = new NgTableParams({}, { dataset: $scope.dashboards});
                 $scope.gridOptions.data = $scope.dashboards;
             }, function errorCallback(response) {
                 window.location.href = "https://" + window.location.hostname;
             });
         }
         /*$scope.getApiversion = function(domain){
             $http({
                 url: '/dashboard/getApiversion?access_token='+localStorage.getItem('access_token')+'&instanceUrl='+localStorage.getItem('instanceUrl')+'&domain='+domain,
                 method: "GET"
             }).then(function successCallback(api_data) {
                     console.log(api_data);
                     $scope.loading = false;
                     $scope.default_api_version = api_data.data.API_Version__c;
                     $scope.getuserprofile();
                 }, function errorCallback(response) {
                 window.location.href = "https://"+window.location.hostname;
             });
         }*/
         $scope.init = function () {
             $http({
                 url: '/oauth2/partnerlogin',
                 method: "GET"
             }).then(function successCallback(partner_data) {
                 console.log(partner_data);
                 if (partner_data.data.access_token) {
                     localStorage.setItem('access_token', partner_data.data.access_token);
                     localStorage.setItem('instanceUrl', partner_data.data.instanceUrl);
                     //$scope.getdashdata();
                     if (localStorage.getItem('cust_profile')) {
                         //let d = "https://"+window.location.hostname;
                         //$scope.getApiversion(d);
                         $scope.getuserprofile();
                     } else {
                         window.location.href = "https://" + window.location.hostname;
                     }
                 } else {
                     //window.location.href = "https://"+window.location.hostname;
                 }
             }, function errorCallback(response) {
                 //window.location.href = "https://"+window.location.hostname;
             });
         }
         $scope.logoutuser = function () {
             $scope.loading = true;
             $http({
                 url: "/users/logout",
                 method: "POST",
                 data: JSON.stringify({ access_token: localStorage.getItem('access_token'), instanceUrl: localStorage.getItem('instanceUrl') })
             }).then(function successCallback(response) {
                 localStorage.clear();
                 window.location.href = "https://" + window.location.hostname;
             }, function errorCallback(response) {
                 alert(response.statusText);
             });
         }
         $scope.createdashboard = function () {
             $scope.loading = true;
             if (!$scope.newdashboardname) {
                 $scope.loading = false;
                 $.notify("Please enter dashboard name", "warn");
                 return false;
             } else {
                 for (var k = 0; k < $scope.datasetdata.length; k++) {
                     let crnt_t = k + 1;
                     console.log(k);
                     console.log($scope.datasetdata.length - 1);
                     console.log($scope.datasetdata[k].metrics.length);
                     if ($scope.datasetdata[k].selecteddim.length > $scope.dashboarddata.No_of_Dimensions__c) {
                         $scope.loading = false;
                         $.notify("Please select " + $scope.dashboarddata.No_of_Dimensions__c + " Dimensions for Dataset " + crnt_t, "warn");
                         k = $scope.datasetdata.length;
                         break;
                     } else if ($scope.datasetdata[k].selecteddate.length > $scope.dashboarddata.No_of_Dates__c) {
                         $scope.loading = false;
                         $.notify("Please select " + $scope.dashboarddata.No_of_Dates__c + " Dates for Dataset " + crnt_t, "warn");
                         k = $scope.datasetdata.length;
                         break;
                     } else if ($scope.datasetdata[k].metrics.length > $scope.dashboarddata.No_of_Measures__c) {
                         $scope.loading = false;
                         $.notify("Please select " + $scope.dashboarddata.No_of_Measures__c + " Measures for Dataset " + crnt_t, "warn");
                         k = $scope.datasetdata.length;
                         break;
                         /*}else if($scope.datasetdata[k].selecteddeffilter == ''){
                             $scope.loading = false;
                             $.notify("Please Enter default filter for Dataset "+crnt_t, "warn");
                             k=$scope.datasetdata.length;
                             break;
                         }*/
                     } else {
                         console.log($scope.datasetdata[k].metrics);
                         console.log($scope.datasetdata[k].metrics.length);
                         if ($scope.datasetdata[k].metrics.length > 0) {
                             for (var m = 0; m < $scope.datasetdata[k].metrics.length; m++) {
                                 if ($scope.datasetdata[k].metrics[m].operation == '' || $scope.datasetdata[k].metrics[m].metricval == '') {
                                     $scope.loading = false;
                                     $.notify("Please fill operation and value for all the fields for the Measure " + m + " Measures for Dataset " + crnt_t, "warn");
                                     m = $scope.datasetdata[k].metrics.length;
                                     k = $scope.datasetdata.length;
                                     break;
                                 } else if (m == $scope.datasetdata[k].metrics.length - 1 && k == $scope.datasetdata.length - 1) {
                                     //$scope.replaceStaticString();
									$scope.getchildTemplates();
                                 }
                             }
                         } else if (k == $scope.datasetdata.length - 1) {
							$scope.getchildTemplates();
                             //$scope.replaceStaticString();
                         }
                     }
                 }
             }
         }
		 $scope.sortres = function(cb){
			$scope.res_arr = _.sortBy($scope.filteredctemp, 'ranking' );
			cb($scope.res_arr);
		 }
		 $scope.getchildTemplates = function(){
			$scope.crnttemp = $scope.selected.dashboard;
			$scope.filteredctemp = [];
			var child_templates = '/dashboard/getChildDashboards?access_token=' + localStorage.getItem('access_token') + '&instanceUrl=' + localStorage.getItem('instanceUrl') + '&dbid=' + $scope.mySelectedRows[0].Id;
			$http({
			 url: child_templates,
			 method: "GET"
			}).then(function successCallback(resp_data) {
				console.log(resp_data);
				if (resp_data.data) {
					if(resp_data.data.length>0 && $scope.dashboarddata && $scope.datasetdata.length>0){
						console.log($scope.datasetdata);
						console.log($scope.dashboarddata);
						$scope.childtemplist = resp_data.data;
						var tem = {};
						tem.seldim = $scope.datasetdata[0].selecteddim.length;
						tem.seldat = $scope.datasetdata[0].selecteddate.length;
						tem.selmes = $scope.datasetdata[0].metrics.length;
						tem.selstr = $scope.datasetdata[0].strings.length;
						if(tem.seldim >= $scope.dashboarddata.No_of_Dimensions__c && tem.seldat >=$scope.dashboarddata.No_of_Dates__c && tem.selmes >= $scope.dashboarddata.No_of_Measures__c && tem.selstr >= $scope.dashboarddata.No_of_Strings__c){
							//$scope.crnttemp = $scope.selected.dashboard;
							$scope.replaceStaticString();
						}else if($scope.childtemplist.length>0){
							for(var m=0;m<$scope.childtemplist.length;m++){
								if(tem.seldim <= $scope.childtemplist[m].No_of_Dimensions__c && tem.seldat<= $scope.childtemplist[m].No_of_Dates__c && tem.selmes <= $scope.childtemplist[m].No_of_Measures__c && tem.selstr <= $scope.childtemplist[m].No_of_Strings__c){
									$scope.filteredctemp.push($scope.childtemplist[m]);
								}
								if(m == $scope.childtemplist.length-1 && $scope.filteredctemp.length == 0){
									$scope.replaceStaticString();
								}else if(m == $scope.childtemplist.length-1 && $scope.filteredctemp.length > 0){
									for(var n=0;n<$scope.filteredctemp.length;n++){
										var resval = parseInt($scope.filteredctemp[n].No_of_Dimensions__c) - parseInt(tem.seldim)+ parseInt($scope.filteredctemp[n].No_of_Dates__c) - parseInt(tem.seldat) + parseInt($scope.filteredctemp[n].No_of_Measures__c) - parseInt(tem.selmes)+parseInt($scope.filteredctemp[n].No_of_Strings__c) - parseInt(tem.selstr);
										$scope.filteredctemp[n].ranking = resval;
										if(n == $scope.filteredctemp.length-1){
											//$scope.crnttemp = res_arr[0].Id;
											$scope.sortres(function(res_arr){
												console.log($scope.res_arr);
												if($scope.res_arr[0].Connect_dashboard_String1__c){
													$scope.dashboarddata.Connect_dashboard_String1__c = $scope.res_arr[0].Connect_dashboard_String1__c;
												}
												if($scope.res_arr[0].Connect_dashboard_String2__c){
													$scope.dashboarddata.Connect_dashboard_String2__c = $scope.res_arr[0].Connect_dashboard_String2__c;
												}
												if($scope.res_arr[0].Connect_dashboard_String3__c){
													$scope.dashboarddata.Connect_dashboard_String3__c = $scope.res_arr[0].Connect_dashboard_String3__c;
												}
												if($scope.res_arr[0].Connect_dashboard_String4__c){
													$scope.dashboarddata.Connect_dashboard_String4__c = $scope.res_arr[0].Connect_dashboard_String4__c;
												}
												if($scope.res_arr[0].Connect_dashboard_String5__c){
													$scope.dashboarddata.Connect_dashboard_String5__c = $scope.res_arr[0].Connect_dashboard_String5__c;
												}
												$scope.replaceStaticString();
											});
										}
									}
								}
							}
						}
					}else{
					  $scope.replaceStaticString();
					}
				}else{
					$scope.replaceStaticString();
				}
			}, function errorCallback(response) {
				$scope.replaceStaticString();
			});
		}
         $scope.get_dataset_name = function (data_set_id) {
             for (var i = 0; i < $scope.datasets.length; i++) {
                 if ($scope.datasets[i].id == data_set_id) {
                     return $scope.datasets[i].name;
                 }
             }
         }
         $scope.get_dataset_label = function (data_set_id) {
             for (var i = 0; i < $scope.datasets.length; i++) {
                 if ($scope.datasets[i].id == data_set_id) {
                     return $scope.datasets[i].label;
                 }
             }
         }
         $scope.get_dashboard_name = function (db_id, field) {
             for (var i = 0; i < $scope.dashboards.length; i++) {
                 if ($scope.dashboards[i].Id == db_id) {
                     console.log('cccc' + $scope.dashboards[i][field]);
                     return $scope.dashboards[i][field];
                 }
             }
         }
         $scope.get_measure_name = function (dset_id, val, type, reqfield) {
             if (type == 'dimension') {
                 if ($scope.datasetdata[dset_id]) {
                     for (var m = 0; m < $scope.datasetdata[dset_id].const_dimensions.length; m++) {
                         if ($scope.datasetdata[dset_id].const_dimensions[m].field == val) {
                             console.log("dimfn" + $scope.datasetdata[dset_id].const_dimensions[m][reqfield]);
                             return $scope.datasetdata[dset_id].const_dimensions[m][reqfield];
                             m = $scope.datasetdata[dset_id].const_dimensions.length;
                             break;
                         }
                     }
                 }
             } else if (type == 'measure') {
                 if ($scope.datasetdata[dset_id]) {
                     for (var m = 0; m < $scope.datasetdata[dset_id].const_measures.length; m++) {
                         if ($scope.datasetdata[dset_id].const_measures[m].field == val) {
                             console.log("mesfn" + $scope.datasetdata[dset_id].const_measures[m][reqfield]);
                             return $scope.datasetdata[dset_id].const_measures[m][reqfield];
                             m = $scope.datasetdata[dset_id].const_measures.length;
                             break;
                         }
                     }
                 }
             } else if (type == 'date') {
                 if ($scope.datasetdata[dset_id]) {
                     for (var m = 0; m < $scope.datasetdata[dset_id].datelist.length; m++) {
                         if ($scope.datasetdata[dset_id].datelist[m].field == val) {
                             return $scope.datasetdata[dset_id].datelist[m][reqfield];
                             m = $scope.datasetdata[dset_id].datelist.length;
                             break;
                         }
                         /*for (var key in $scope.datasetdata[dset_id].dates[m].fields) {
                             if ($scope.datasetdata[dset_id].dates[m].fields.hasOwnProperty(key)) {
                                 if($scope.datasetdata[dset_id].dates[m].fields[key] == val){
                                     let ret_val = $scope.datasetdata[dset_id].dates[m].fields['reqfield'];
                                     console.log(ret_val);
                                     m = $scope.datasetdata[dset_id].dates.length;
                                     return ret_val;
                                     break;
                                 }
                             }
                         }*/
                     }
                 }
             }else if(type == 'unique'){
				if ($scope.datasetdata[dset_id]) {
					for (var m = 0; m < $scope.datasetdata[dset_id].const_dimensions.length; m++) {
                         if ($scope.datasetdata[dset_id].const_dimensions[m].field == val) {
                             console.log("dimfn" + $scope.datasetdata[dset_id].const_dimensions[m][reqfield]);
                             return $scope.datasetdata[dset_id].const_dimensions[m][reqfield];
                             m = $scope.datasetdata[dset_id].const_dimensions.length;
                             break;
                         }
						 if(m == $scope.datasetdata[dset_id].const_dimensions.length-1){
							for (var n = 0; n < $scope.datasetdata[dset_id].const_measures.length; n++) {
								 if ($scope.datasetdata[dset_id].const_measures[n].field == val) {
									 console.log("mesfn" + $scope.datasetdata[dset_id].const_measures[n][reqfield]);
									 return $scope.datasetdata[dset_id].const_measures[n][reqfield];
									 n = $scope.datasetdata[dset_id].const_measures.length;
									 break;
								 }
							}
						 }
                    }
					
				}
			 }
         }
         $scope.addmetrow = function (ind) {
             if ($scope.datasetdata[ind].metrics.length >= $scope.dashboarddata.No_of_Measures__c) {
                 $.notify("Exceeded Maximum Measures to be selected", "warn");
             } else {
                 let metricinit = {
                     operation: '',
                     metricval: '',
                     metrictext: ''
                 };
                 $scope.datasetdata[ind].metrics.push(metricinit);
             }
         }
         $scope.addmestrow = function (ind) {
             if ($scope.datasetdata[ind].strings.length >= $scope.dashboarddata.No_of_Strings__c) {
                 $.notify("Exceeded Maximum Strings to be added", "warn");
             } else {
                 $scope.datasetdata[ind].strings.push('');
             }
         }
         $scope.removemestrow = function (pind, ind) {
             console.log($scope.datasetdata);
             $scope.datasetdata[pind].strings.splice(ind, 1);
         }
         $scope.removemetrow = function (pind, ind) {
             let temp = pind + '-' + ind;
             console.log(temp);
             $scope.datasetdata[pind].metrics.splice(ind, 1);
             //$scope.datasetdata[pind].measuretext.splice(ind,1);
             console.log($scope.datasetdata[pind].metrics);
             /*for(var i=0;i<$scope.datasetdata[pind].measuretext.length;i++){
                 if($scope.datasetdata[pind].measuretext[i].id == temp){
                     $scope.datasetdata[pind].measuretext.splice(i,1);
                 }
             }*/
         }
         $scope.metriccallout = function (data_set_id, met_url, versionId,vertype) {
             if (met_url && localStorage.getItem('cust_access_token')) {
                 send_obj = {};
                 send_obj.access_token = localStorage.getItem('cust_access_token');
                 send_obj.url = met_url;
				 send_obj.type = vertype;
				 send_obj.domain_url = localStorage.getItem('cust_instanceUrl');
                 $http({
                     url: '/dashboard/connecturl',
                     method: "POST",
                     data: JSON.stringify(send_obj)
                 }).then(function successCallback(dimension_data) {
                     dimension_data.data = JSON.parse(dimension_data.data);
                     if (dimension_data) {
                         for (var i = 0; i < dimension_data.data.dimensions.length; i++) {
                             dimension_data.data.dimensions[i].category = 'DIMENSIONS';
                         }
                         for (var j = 0; j < dimension_data.data.measures.length; j++) {
                             dimension_data.data.measures[j].category = 'MEASURES';
                         }
                         var temp_obj = {};
                         temp_obj.dataset_id = data_set_id;
                         temp_obj.dataset_name = $scope.get_dataset_name(data_set_id);
                         temp_obj.dataset_label = $scope.get_dataset_label(data_set_id);
                         temp_obj.dimensions = dimension_data.data.dimensions;
                         temp_obj.const_dimensions = angular.copy(dimension_data.data.dimensions);
                         temp_obj.dates = dimension_data.data.dates;
                         temp_obj.versionId = versionId;
                         temp_obj.measures = dimension_data.data.measures;
                         temp_obj.const_measures = angular.copy(dimension_data.data.measures);
                         temp_obj.uniquefields = dimension_data.data.dimensions;
                         temp_obj.uniquefields = temp_obj.uniquefields.concat(dimension_data.data.measures);
                         temp_obj.const_uniquefields = angular.copy(temp_obj.uniquefields);
                         temp_obj.userSearch = '';
                         temp_obj.metrics = [];
                         temp_obj.strings = [];
                         temp_obj.selecteddeffilter = '';
                         temp_obj.selecteddim = [];
                         temp_obj.selecteddate = [];
                         temp_obj.removedimepoch = true;
                         temp_obj.removemesepoch = true;
                         //temp_obj.measuretext = [];
                         temp_obj.datelist = [];
                         temp_obj.dim_order = 'label';
						 temp_obj.dim_direction = true;
                         /*let metricinit = {
                             operation: '',
                             metricval: ''
                         };
                         temp_obj.metrics.push(metricinit);*/
                         var dateselist = [];
                         /*for(var i=0;i<temp_obj.dates.length;i++){
                             let curnt_date = temp_obj.dates[i].fields;
                             $.each(curnt_date, function(key, value) {
                                 dateselist.push(value);
                             });
                         }*/
                         for (var dt = 0; dt < dimension_data.data.dates.length; dt++) {
                             var dttemp = {};
                             dttemp.label = dimension_data.data.dates[dt].label;
                             console.log(dttemp.label);
                             for (var key in dimension_data.data.dates[dt].fields) {
                                 console.log(key);
                                 if (key == 'fullField') {
                                     dttemp.field = dimension_data.data.dates[dt].fields['fullField'];
                                     console.log(dimension_data.data.dates[dt].fields['fullField']);
                                     temp_obj.datelist.push(dttemp);
                                 }
                             }
                             console.log(temp_obj.datelist);
                         }
                         $scope.datasetdata.push(temp_obj);
						 /*if($scope.dashboarddata.Dataset_Card_Util__c){
							$scope.showotherprops = true;
							$scope.getdimproperties($scope.datasetdata[$scope.datasetdata.length-1]);
						 }*/
						 if($scope.org_user.Card_Util__c){
							$scope.showotherprops = true;
							$scope.getdimproperties($scope.datasetdata[$scope.datasetdata.length-1]);
						 }else{
							$scope.showotherprops = false;
							$scope.gridOptions.data = $scope.dashboards
						 }
                         console.log($scope.datasetdata);
                         $scope.orignaldatset_data = angular.copy($scope.datasetdata);
                         $scope.loading = false;
                         $scope.datasetsec = false;
                         $scope.metricsec = true;
						 if(!$scope.prevselected  || $scope.prevselected.length == 0){
							for(var q=0;q<$scope.dashboarddata.No_of_Measures__c;q++){
								var tm = {};
								console.log(q);
								tm.operation = '';
								tm.metricval = '';
								$scope.datasetdata[$scope.datasetdata.length-1].metrics.push(tm);
								console.log($scope.datasetdata[$scope.datasetdata.length-1]);
							}
							for(var p=0;p<$scope.dashboarddata.No_of_Strings__c;p++){
								console.log(p);
								$scope.datasetdata[$scope.datasetdata.length-1].strings.push('');
								
							}
						}
                        $timeout(function () {
                             for (var h = 0; h < $scope.datasetdata.length; h++) {
                                 $scope.updatepoch(h, 'dimension');
                                 $scope.updatepoch(h, 'measure');
                             }
                             if ($scope.prevselected && $scope.prevselected.length > 0) {
                                 var prev_selections = JSON.parse($scope.prevselected[0].Selected_Values__c);
                                 console.log(prev_selections);
                                 for (var m = 0; m < $scope.datasetdata.length; m++) {
                                     console.log('m1');
                                     for (var p = 0; p < prev_selections.length; p++) {
                                         console.log('mm' + $scope.datasetdata[m].dataset_id);
                                         console.log('mma' + prev_selections[p].dataset_id);
                                         if ($scope.datasetdata[m].dataset_id == prev_selections[p].dataset_id) {
                                             console.log('matched');
											 //console.log($scope.selected);
											 //alert($scope.selected);
                                             if (prev_selections[p].selecteddim) {
												console.log(prev_selections[p].selecteddim);
												$scope.datasetdata[m].selecteddim = prev_selections[p].selecteddim;
												$timeout(function(){
													$scope.$broadcast('updateselect');
												},1000);
                                             }
                                             if (prev_selections[p].selecteddate) {
                                                 $scope.datasetdata[m].selecteddate = prev_selections[p].selecteddate;
                                                 console.log($scope.datasetdata[m].selecteddate);
                                             }
                                             if (prev_selections[p].metrics) {
                                                 $scope.datasetdata[m].metrics = prev_selections[p].metrics;
                                                 console.log($scope.datasetdata[m].metrics);
                                             }
                                             if (prev_selections[p].strings) {
                                                 $scope.datasetdata[m].strings = prev_selections[p].strings;
                                                 console.log($scope.datasetdata[m].strings);
                                             }
                                         }
                                     }
                                 }
                             }else{
								console.log($scope.dashboarddata.No_of_Measures__c);
								
							 }
                             //window.attachev();
                         }, 100);
                     }
                 }, function errorCallback(response) {
                     $scope.loading = false;
                     $scope.error = response.statusText;
                 });
             }
         }
         $scope.getfolders = function () {
             $scope.loading = true;
             let foldrfetch_url = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/folders?hasCurrentOnly=true';
             if (localStorage.getItem('cust_instanceUrl')) {
                 send_obj = {};
                 send_obj.access_token = localStorage.getItem('cust_access_token');
                 send_obj.url = foldrfetch_url;
                 $http({
                     url: '/dashboard/connecturl',
                     method: "POST",
                     data: JSON.stringify(send_obj)
                 }).then(function successCallback(foldr_data) {
                     foldr_data = JSON.parse(foldr_data.data);
                     if (foldr_data) {
                         $scope.loading = false;
                         $scope.folder_data = [];
                         for (var f = 0; f < foldr_data.folders.length; f++) {
                             if (foldr_data.folders[f].permissions.manage || foldr_data.folders[f].permissions.modify) {
                                 $scope.folder_data.push(foldr_data.folders[f]);
                             }
                         }
                         if ($scope.user_profile && $scope.user_profile.user_id) {
                             let tem = {};
                             tem.id = $scope.user_profile.user_id;
                             tem.name = 'My Private App';
                             $scope.folder_data.push(tem);
                         }
                         $scope.selected.folder = 'noval';
                         $scope.selected.dashoption = 'noval';
                         //$scope.folder_data = foldr_data.data.folders;
                         console.log($scope.folder_data);
                         $.notify("Connected to Customer Instance", "success");
                         $.notify("Datasets fetched", "success");
                     }
                 }, function errorCallback(response) {
                     $scope.loading = false;
                     console.log(response.statusText);
                 });
             }
         }
         $scope.fetchdatasets = function (dataseturl) {
             if (localStorage.getItem('cust_instanceUrl')) {
                 var fetch_url = localStorage.getItem('cust_instanceUrl') + dataseturl;
                 send_obj = {};
                 send_obj.access_token = localStorage.getItem('cust_access_token');
                 send_obj.url = fetch_url;
                 $http({
                     url: '/dashboard/connecturl',
                     method: "POST",
                     data: JSON.stringify(send_obj)
                 }).then(function successCallback(resp_data) {
                     console.log(resp_data);
                     resp_data = JSON.parse(resp_data.data);
                     if (resp_data) {
                         if ($scope.datasets) {
                             $scope.datasets = $scope.datasets.concat(resp_data.datasets);
                         } else {
                             $scope.datasets = resp_data.datasets;
                         }
                         console.log($scope.datasets);
						 if (resp_data.nextPageUrl == null) {
                             console.log('ccccin');
                             $scope.dashboardsec = false;
                             $scope.datasetsec = true;
                             $scope.getfolders();
                         } else {
                            // $scope.fetchdatasets(resp_data.nextPageUrl);
							$scope.dashboardsec = false;
                             $scope.datasetsec = true;
                             $scope.getfolders();
                         }
					 }
                 }, function errorCallback(response) {
                     console.log(response);
                     if (response == null) {
                         $.notify("No Datasets Found", "warn");
                     }
                     $scope.loading = false;
                 });
             } else {
                 localStorage.clear();
                 window.location.href = "https://" + window.location.hostname;
             }
         }
         $scope.fetchcustomerdatasets = function () {
             if (localStorage.getItem('cust_access_token') && localStorage.getItem('cust_instanceUrl')) {
                 $scope.fetchdatasets('/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/datasets?pageSize=200');
             }
         }
         $scope.create_user = function () {
             var user_data = {};
             user_data.data = $scope.user_profile;
             user_data.instance_url = localStorage.getItem('instanceUrl');
             user_data.access_token = localStorage.getItem('access_token');
             $http({
                 url: '/users/createuser/',
                 method: "POST",
                 data: JSON.stringify(user_data)
             }).then(function successCallback(response) {
                 console.log(response);
                 var permission = [];
                 $scope.org_user = response.data;
                 if (response.data.Dashboard_Type_Access__c) {
                     permission = response.data.Dashboard_Type_Access__c.split(";");
                 } else {
					$scope.org_user.Dashboard_Type_Access__c = 'Free';
                     permission.push('Free');
                 }
                 $scope.getdashdata(permission);
             }, function errorCallback(response) {
                 console.log(response);
             });
         }
         $scope.fetchdashboarddata = function () {
             $scope.loading = true;
             if (localStorage.getItem('access_token') && localStorage.getItem('instanceUrl')) {
                 $scope.mySelectedRows = $scope.gridApi1.selection.getSelectedRows();
                 console.log($scope.mySelectedRows);
                 if ($scope.mySelectedRows[0] && !$scope.mySelectedRows[0].No_Selections__c) {
                     $scope.selected.dashboard = $scope.mySelectedRows[0].Id;
                     var prev_dashboard_url = '/dashboard/getPreviousDashboards?access_token=' + localStorage.getItem('access_token') + '&instanceUrl=' + localStorage.getItem('instanceUrl') + '&conid=' + $scope.org_user.Id + '&temid=' + $scope.selected.dashboard;
                     $http({
                         url: prev_dashboard_url,
                         method: "GET"
                     }).then(function successCallback(prev_data) {
                         $scope.prevdashboards = prev_data.data;
                         if ($scope.prevdashboards.length > 0) {
                             $scope.dashboardoptions = ['New', 'Existing'];
                         } else {
                             $scope.dashboardoptions = ['New'];
                         }
                         $scope.gridOptions2.data = $scope.prevdashboards;
                         var get_details = '/dashboard/getDashboarddata?access_token=' + localStorage.getItem('access_token') + '&instanceUrl=' + localStorage.getItem('instanceUrl') + '&dbid=' + $scope.mySelectedRows[0].Id;
                         $http({
                             url: get_details,
                             method: "GET"
                         }).then(function successCallback(resp_data) {
                             if (resp_data) {
                                 console.log(resp_data);
                                 let tem = JSON.parse(JSON.stringify(resp_data.data));
                                 if (tem.length > 0) {
                                    $scope.dashboarddata = tem[0];
                                     console.log($scope.dashboarddata);
									$scope.fetchcustomerdatasets();
                                 } else {
                                     $.notify("No Dashboard data found", "warn");
                                 }
                             }
                         }, function errorCallback(response) {
                             $scope.loading = false;
                             $scope.error = response.statusText;
                             console.log($scope.error);
                         });
                     }, function errorCallback(response) {
                         $scope.loading = false;
                         $scope.error = response.statusText;
                         console.log($scope.error);
                     });
                 } else if($scope.mySelectedRows[0] && $scope.mySelectedRows[0].No_Selections__c){
					 $scope.dashboardsec = false;
					 $scope.loading = false;
					 $scope.uploadsec = true;
					 $scope.datasetloading =  true;
					 $scope.datasetloaded = false;
					 $scope.dashboardloading = false;
					 var prev_dashboard_url = '/dashboard/uploadautodata?access_token=' + localStorage.getItem('access_token') + '&instanceUrl=' + localStorage.getItem('instanceUrl') + '&user_access_token=' + localStorage.getItem('cust_access_token') + '&user_instance_url='+ localStorage.getItem('cust_instanceUrl')+'&template_id='+$scope.mySelectedRows[0].Id;
                     $http({
                         url: prev_dashboard_url,
                         method: "GET"
                     }).then(function successCallback(res) {
						console.log(res);
						$scope.datasetloading =  false;
						console.log(res);
						if(res.status == '200'){
							$scope.datasetloaded = true;
							$scope.dashboardloading = true;
							console.log('-00000000000000000');
							console.log($scope.dashboarddata);
							$.notify("Dataset created succesfully", "success");
                            // How would I add ' + $scope.dashboarddata.API_Version__c + ' instead of v47.0
                            var create_url = localStorage.getItem('cust_instanceUrl') + '/services/data/v47.0/wave/dashboards';
							var send_obj = {};
							send_obj.customerurl = create_url;
							send_obj.template_id = $scope.mySelectedRows[0].Id;
							send_obj.cust_access_token = localStorage.getItem('cust_access_token');
							send_obj.org_access_token =  localStorage.getItem('access_token');
							send_obj.org_url =  localStorage.getItem('instanceUrl');
							send_obj.user_id = $scope.user_profile.user_id;
							$http({
								 url: '/dashboard/createdb',
								 method: "POST",
								 data: JSON.stringify(send_obj)
							 }).then(function successCallback(res) {
								  console.log(res);
								  $scope.dashboardloading = false;
								  if(res){
									$.notify("Dashoboard created succesfully", "success");
                                    $scope.dashboardloaded = true;									
								  }
								}, function errorCallback(response) {
									$scope.dashboardloading = false;
							 });
						}
					 },function errorCallback(response) {
						console.log('err');
						$scope.datasetloading =  false;
						console.log(response);
					 });
				 }else {
                     $scope.loading = false;
                     $.notify("Please select a dashboard", "warn");
                 }
             } else {
                 window.location.href = "https://" + window.location.hostname;
             }
         };
		 /*$scope.createDataflow = function(){
			if($scope.dataflowlabel!= '' && $scope.dashboarddata.Connect_Dataflow_String__c && localStorage.getItem('cust_access_token')!= ''){
				var t_ob = {};
				t_ob.label = $scope.dataflowlabel;
				t_obj.url = localStorage.getItem('cust_instanceUrl')+'/services/data/'+$scope.dashboarddata.API_Version__c;
				t_ob.json_str = $scope.dashboarddata.Connect_Dataflow_String__c;
				t_ob.access_token = localStorage.getItem('cust_access_token');
				 $http({
                     url: '/dataflow/initiate',
                     method: "POST",
                     data: JSON.stringify(t_ob)
                 }).then(function successCallback(response) {
                     console.log(response)
                     $scope.loading = false;
                     $.notify("created succesfully", "success");
                     console.log(response);
                 }, function errorCallback(response) {
                     $scope.loading = false;
                     console.log(response);
                     $.notify("Error creating Dashboard " + response.data[0].message, "warn");
                     //localStorage.clear();
                     //window.location.href = "https://"+window.location.hostname;
                 });
			}
			
		 }*/
		 $scope.getdimproperties = function(objct){
			if (localStorage.getItem('cust_instanceUrl')) {
                var prop_url = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/query';
				if(objct.dataset_id && objct.dimensions){
					var final_str = {};
					var gen_str = "r = load \"" + objct.dataset_id + "/" + objct.versionId + "\";"; 
					var totalqueries = [];
					 var p_obj = {};
					 p_obj.access_token = localStorage.getItem('cust_access_token');
					 p_obj.url = prop_url;
					for(var m=0;m<objct.dimensions.length;m++){
						var curntin = m+1;
						gen_str += "q"+curntin+" = group r by '"+ objct.dimensions[m].field +"';";
						gen_str += "q"+curntin+" = foreach q"+curntin+" generate '"+ objct.dimensions[m].field +"' as '"+ objct.dimensions[m].field +"', count() as 'count';";
						gen_str += "q"+curntin+" = group q"+curntin+" by 'all',r by 'all';";
						gen_str += "q"+curntin+" = foreach q"+curntin+" generate \""+ objct.dimensions[m].field +"\" as 'field',unique(r.'"+ objct.dimensions[m].field +"') as Uniqueness, sum(q"+curntin+".'count')/count(r)*100 as 'Completeness';";
						totalqueries.push("q"+curntin);
						if(m == objct.dimensions.length-1){
							gen_str += "final = union "+totalqueries.join()+";";
							gen_str +="final = group final by 'field';";
							gen_str +="foreach final generate field, sum(Uniqueness) as Card, sum(Completeness) as Util;";
							final_str.query = gen_str;
							p_obj.qury = JSON.stringify(final_str);
							 $http({
								 url: '/dashboard/getotherproperties',
								 method: "POST",
								 data: JSON.stringify(p_obj)
							 }).then(function successCallback(resp) {
								console.log(resp);
								var rep = JSON.parse(resp.data.body);
								console.log(rep);
								if(rep.results && rep.results.records){
									for(var b=0;b<objct.dimensions.length;b++){
										for(var t=0;t<rep.results.records.length;t++){
											if(rep.results.records[t].field == objct.dimensions[b].field){
												if(rep.results.records[t].Card && rep.results.records[t].Util){
													objct.dimensions[b].card = rep.results.records[t].Card;
													objct.dimensions[b].uniq = rep.results.records[t].Util;
													console.log('one'+objct.dimensions[b].label);
													objct.dimensions[b].proplabel = objct.dimensions[b].label+' | Uniqueness: '+objct.dimensions[b].card+' | Completeness: '+objct.dimensions[b].uniq;
													break;
												}else{
													if(rep.results.records[t].Card){
														objct.dimensions[b].card = rep.results.records[t].Card;
													}else{
														objct.dimensions[b].card = 'N/A';
													}
													if(rep.results.records[t].Util){
														objct.dimensions[b].uniq = rep.results.records[t].Util;
													}else{
														objct.dimensions[b].uniq = 'N/A';
													}
													console.log('last'+objct.dimensions[b].label);
													objct.dimensions[b].proplabel = objct.dimensions[b].label;
													break;
												}
											}else if(t == rep.results.records.length-1){
												console.log('la'+objct.dimensions[b].label);
												objct.dimensions[b].card = 'N/A';
												objct.dimensions[b].uniq = 'N/A';
												objct.dimensions[b].proplabel = objct.dimensions[b].label;
											}
										}
									}
									for(var s=0;s<objct.const_dimensions.length;s++){
										for(var q=0;q<rep.results.records.length;q++){
											if(rep.results.records[q].field == objct.const_dimensions[s].field){
												if(rep.results.records[q].Card && rep.results.records[q].Util){
													objct.const_dimensions[s].card = rep.results.records[q].Card;
													objct.const_dimensions[s].uniq = rep.results.records[q].Util;
													objct.const_dimensions[s].proplabel = objct.const_dimensions[s].label+' | Uniqueness: '+objct.const_dimensions[s].card+' | Completeness: '+objct.const_dimensions[s].uniq;
													break;
												}else{
													if(rep.results.records[q].Card){
														objct.const_dimensions[s].card = rep.results.records[q].Card;
													}else{
														objct.const_dimensions[s].card = 'N/A';
													}
													if(rep.results.records[q].Util){
														objct.const_dimensions[s].uniq = rep.results.records[q].Util;
													}else{
														objct.const_dimensions[s].uniq = 'N/A';
													}
													objct.const_dimensions[s].proplabel = objct.const_dimensions[s].label;
													break;
												}
											}else if(q==rep.results.records.length-1){
												objct.const_dimensions[s].card = 'N/A';
												objct.const_dimensions[s].uniq = 'N/A';
												objct.const_dimensions[s].proplabel = objct.const_dimensions[s].label;
											}
										}
									}
								}
								console.log($scope.datasetdata);
							 }, function errorCallback(response) {
								console.log(response);
							 });
						}
					}
					console.log(gen_str);
					console.log(final_str);
				}
			}
		 }
		
         $scope.getmetrics = function (type) {
             $scope.datasetdata = [];
             if (type == 'dataset') {
                 if (localStorage.getItem('cust_access_token') && localStorage.getItem('cust_instanceUrl') && $scope.datasets.length > 0) {
                     let dataset_res = $scope.selected.dataset;
                     if (dataset_res.length > 0) {
                         for (var i = 0; i < dataset_res.length; i++) {
                             for (var x = 0; x < $scope.datasets.length; x++) {
                                 console.log($scope.datasets[x].currentVersionId);
								 console.log($scope.datasets[x].id );
								 console.log(dataset_res[i]);
                                 if ($scope.datasets[x].id == dataset_res[i] && $scope.datasets[x].currentVersionId) {
                                    var fetch_dim = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/datasets/' + $scope.datasets[x].id + '/versions/' + $scope.datasets[x].currentVersionId + '/xmds/main';
                                    $scope.metriccallout($scope.datasets[x].id, fetch_dim, $scope.datasets[x].currentVersionId,'wversion');
                                 }else if($scope.datasets[x].id == dataset_res[i]){
									var fetch_dim = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/datasets/' + $scope.datasets[x].id + '/versions/';
									$scope.metriccallout($scope.datasets[x].id, fetch_dim, $scope.datasets[x].currentVersionId,'nversion');
								 }
                             }
							 /*if(i == dataset_res.length-1){
								 $scope.UploadDataset();
							 }*/
                         }
                     } else {
                         $scope.loading = false;
                         $.notify("Choose a valid dataset", "warn");
                     }
                 } else {
                     $scope.loading = false;
                     $.notify("No datasets found/Customer AccessToken Missing", "warn");
                 }
             } else if (type == 'prevdashboard') {
                 if (localStorage.getItem('cust_access_token') && localStorage.getItem('cust_instanceUrl') && $scope.prevselected.length > 0) {
                     var pv_selections = JSON.parse($scope.prevselected[0].Selected_Values__c);
                     var temp_ar = [];
                     if ($scope.dashboarddata.No_of_Datasets__c == pv_selections.length) {
                         for (var k = 0; k < pv_selections.length; k++) {
                             for (var x = 0; x < $scope.datasets.length; x++) {
                                 if (pv_selections[k].dataset_id == $scope.datasets[x].id && $scope.datasets[x].currentVersionId) {
                                     var tbjo = {};
                                     tbjo.dataset_id = $scope.datasets[x].id;
                                     tbjo.currentVersionId = $scope.datasets[x].currentVersionId;
                                     temp_ar.push(tbjo);
                                 }
                             }
                             if (k == pv_selections.length - 1) {
                                 if (temp_ar.length == $scope.dashboarddata.No_of_Datasets__c) {
                                     for (var t = 0; t < temp_ar.length; t++) {
                                         var fetch_dim = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/datasets/' + temp_ar[t].dataset_id + '/versions/' + temp_ar[t].currentVersionId + '/xmds/main';
                                         $scope.metriccallout(temp_ar[t].dataset_id, fetch_dim, temp_ar[t].currentVersionId,'wversion');
                                     }
                                 } else {
                                     $scope.loading = false;
                                     $.notify("Mismatch in Template Datasets to required dasboard datasets", "warn");
                                 }
                             }
                         }
                     } else {
                         $scope.loading = false;
                         $.notify("Mismatch with Number of Datasets in Previous Dashboard to Number of datasets required in the template", "warn");
                     }
                 } else {
                     $scope.loading = false;
                     /*$timeout(function(){
                         window.location.href = "https://"+window.location.hostname;
                     },500);*/
                     $.notify("No datasets found/Customer AccessToken Missing", "warn");
                 }
             }
         }
         $scope.validatedataset = function () {
             $scope.loading = true;
             let count = $scope.selected.dataset.length;
             console.log($scope.selected.dataset);
             if ($scope.selected.dashoption && $scope.selected.dashoption != 'noval') {
                 if (count > 0 && (!$scope.gridApi2 || !$scope.gridApi2.selection || $scope.gridApi2.selection.getSelectedRows().length == 0)) {
                     if (count == $scope.dashboarddata.No_of_Datasets__c) {
                         if($scope.selected.datasetfile && !$scope.selected.folder){
							 $scope.loading = false;
                             $.notify("Please choose a folder", "warn");
						 }else if ($scope.selected.folder && $scope.selected.folder != 'noval') {
                             $scope.getmetrics('dataset');
                         } else {
                             $scope.loading = false;
                             $.notify("Please choose a folder", "warn");
                         }
                     } else {
                         $scope.loading = false;
                         $.notify($scope.dashboarddata.No_of_Datasets__c + ' Datasets are required', "warn");
                         //alert($scope.dashboarddata.No_of_Datasets__c+ 'Datasets are required');
                     }
                 } else if ($scope.gridApi2 && $scope.gridApi2.selection.getSelectedRows().length > 0 && count == 0) {
					$scope.prevselected = $scope.gridApi2.selection.getSelectedRows();
					console.log($scope.prevselected);
                    if($scope.selected.datasetfile && !$scope.selected.folder){
						$scope.loading = false;
						$.notify("Please choose a folder", "warn");
					}else if ($scope.selected.folder) {
                        $scope.getmetrics('prevdashboard');
                    } else {
                        $scope.loading = false;
                        $.notify("Please choose a folder", "warn");
                    }
                 } else if ($scope.gridApi2 && $scope.gridApi2.selection.getSelectedRows().length > 0 && count > 0) {
                     $scope.loading = false;
                     $.notify("Choose any of previous or datasets. Not both.", "warn");
                 } else {
                     $scope.loading = false;
                     $.notify("Choose a Dataset or Previous dashboard selection", "warn");
                     //alert('notselected');
                 }
             } else {
                 $scope.loading = false;
                 $.notify("Choose an option to create a Dashboard", "warn");
             }
         }
         $scope.checkifexists = function (arr, value) {
             for (var i = 0, iLen = arr.length; i < iLen; i++) {
                 if (arr[i].metrictext == value) {
                     i = arr.length;
                     return true;
                     break;
                 } else if (i == arr.length - 1) {
                     return false;
                     break;
                 }
             }
         }
         $scope.updatemeasure = function (pind, ind) {
             let temp = pind + '-' + ind;
             var temp_obj = {};
             temp_obj.id = temp;
             if ($scope.datasetdata[pind].metrics[ind].operation != '') {
                 if ($scope.datasetdata[pind].metrics[ind].operation == 'count' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'count(*)';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'sum' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'sum(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'avg' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'avg(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'max' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'max(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'min' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'min(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'unique' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'unique(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'median' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'median(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'first' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'first(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'last' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'last(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'stddev' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'stddev(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'stddevp' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'stddevp(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'var' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'var(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'varp' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = 'varp(' + $scope.datasetdata[pind].metrics[ind].metricval + ')';
                 } else if ($scope.datasetdata[pind].metrics[ind].operation == 'calculate' && $scope.datasetdata[pind].metrics[ind].metricval != '') {
                     temp_obj['val'] = $scope.datasetdata[pind].metrics[ind].metricval;
                 }
                 /*if($scope.datasetdata[pind].measuretext.length>0){
                     if($scope.datasetdata[pind].measuretext.indexOf(temp_obj['val'])!= -1){
                         $.notify("Metric Already exists", "warn");
                     }else if(ind<$scope.datasetdata[pind].measuretext.length){
                         $scope.datasetdata[pind].metrics[ind].measuretext = temp_obj['val'];
                     }else{
                         $scope.datasetdata[pind].measuretext.push(temp_obj['val']);
                     }
                 }else{
                     $scope.datasetdata[pind].measuretext.push(temp_obj['val']);
                 }*/
                 if ($scope.checkifexists($scope.datasetdata[pind].metrics, temp_obj['val'])) {
                     $.notify("Metric Already exists", "warn");
                 } else {
                     $scope.datasetdata[pind].metrics[ind].metrictext = temp_obj['val'];
                 }
             }
             console.log($scope.datasetdata[pind]);
         }
         $scope.manipulatestring = function (cb) {
             var temp_string = '';
             for (var m = 0; m < 5; m++) {
                 var cnt = m + 1;
                 let tempfld = 'Connect_dashboard_String' + cnt + '__c';
                 if ($scope.dashboarddata[tempfld] != '' && $scope.dashboarddata[tempfld] != null) {
                     temp_string += $scope.dashboarddata[tempfld];
                 }
             }
             $scope.saveobj.dsdata = [];
             console.log($scope.selected.dashboard);
             let pexp = '#NewFolderId#';
             let pexp2 = '#DashboardName#';
             let pexp3 = '#DashboardDescription#';
             let pexp4 = "#DashboardTitle#";
             temp_string = temp_string.split(pexp).join($scope.selected.folder);
             temp_string = temp_string.split(pexp2).join($scope.newdashboardname);
             temp_string = temp_string.split(pexp3).join('');
             temp_string = temp_string.split(pexp4).join($scope.newdashboardname);
             for (var k = 0; k < $scope.datasetdata.length; k++) {
                 var curnt_cnt = k + 1;
                 var t_obj = {};
                 t_obj.dataset_id = $scope.datasetdata[k].dataset_id;
                 t_obj.dataset_name = $scope.datasetdata[k].dataset_name;
                 let exp = '#' + curnt_cnt + 'DatasetId#';
                 let exp2 = '#' + curnt_cnt + 'DatasetAlias#';
                 temp_string = temp_string.split(exp).join($scope.datasetdata[k].dataset_id);
                 temp_string = temp_string.split(exp2).join($scope.datasetdata[k].dataset_name);
                 if ($scope.datasetdata[k].selecteddim.length > 0) {
                     t_obj.selecteddim = [];
                     for (var d = 0; d < $scope.datasetdata[k].selecteddim.length; d++) {
                         var curnt_temp = d + 1;
						 var ttmp = {};
						 ttmp.label = $scope.datasetdata[k].selecteddim[d].label;
						 ttmp.field = $scope.datasetdata[k].selecteddim[d].field;
                         let exp3 = '#' + curnt_cnt + 'label' + curnt_temp + '#';
                         let exp4 = '#' + curnt_cnt + 'alias' + curnt_temp + '#';
                         let exp301 = '#' + curnt_cnt + 'DimensionLabel' + curnt_temp + '#';
                         let exp401 = '#' + curnt_cnt + 'DimensionAlias' + curnt_temp + '#';
                         temp_string = temp_string.split(exp3).join($scope.datasetdata[k].selecteddim[d].label);
                         temp_string = temp_string.split(exp4).join($scope.datasetdata[k].selecteddim[d].field);
                         temp_string = temp_string.split(exp301).join($scope.datasetdata[k].selecteddim[d].label);
                         temp_string = temp_string.split(exp401).join($scope.datasetdata[k].selecteddim[d].field);
						 t_obj.selecteddim.push(ttmp);
                     }
                 }
                 if ($scope.datasetdata[k].selecteddate.length > 0) {
                     t_obj.selecteddate = $scope.datasetdata[k].selecteddate;
                     for (var d = 0; d < $scope.datasetdata[k].selecteddate.length; d++) {
                         var curnt_temp = d + 1;
                         let exp5 = '#' + curnt_cnt + 'datelabel' + curnt_temp + '#';
                         let exp6 = '#' + curnt_cnt + 'datealias' + curnt_temp + '#';
                         let exp501 = '#' + curnt_cnt + 'DateLabel' + curnt_temp + '#';
                         let exp601 = '#' + curnt_cnt + 'DateAlias' + curnt_temp + '#';
                         console.log('------------' + exp501);
                         console.log('------------' + $scope.get_measure_name(k, $scope.datasetdata[k].selecteddate[d], 'date', 'label'));
                         temp_string = temp_string.split(exp5).join($scope.get_measure_name(k, $scope.datasetdata[k].selecteddate[d], 'date', 'label'));
                         temp_string = temp_string.split(exp6).join($scope.datasetdata[k].selecteddate[d]);
                         temp_string = temp_string.split(exp501).join($scope.get_measure_name(k, $scope.datasetdata[k].selecteddate[d], 'date', 'label'));
                         temp_string = temp_string.split(exp601).join($scope.datasetdata[k].selecteddate[d]);
                     }
                 }
                 if ($scope.datasetdata[k].metrics.length > 0) {
                     t_obj.metrics = $scope.datasetdata[k].metrics;
                     for (var d = 0; d < $scope.datasetdata[k].metrics.length; d++) {
                         var curnt_temp = d + 1;
                         let exp7 = '#' + curnt_cnt + 'MeasureLabel' + curnt_temp + '#';
                         let exp8 = '#' + curnt_cnt + 'MeasureExpression' + curnt_temp + '#';
                         let exp701 = '#' + curnt_cnt + 'MeasureFunctionLabel' + curnt_temp + '#';
                         let exp9 = '#' + curnt_cnt + 'MeasureAlias' + curnt_temp + '#';
                         let exp901 = '#' + curnt_cnt + 'MeasureAliasCompact' + curnt_temp + '#';
                         let exp801 = '#' + curnt_cnt + 'MeasureFunction' + curnt_temp + '#';
                         let exp10 = '[\\"#' + curnt_cnt + 'CompactMeasureAPI' + curnt_temp + '#\\"]';
                         if ($scope.datasetdata[k].metrics[d].operation != 'count') {
							if($scope.datasetdata[k].metrics[d].operation == 'unique'){
								temp_string = temp_string.split(exp7).join($scope.datasetdata[k].metrics[d].operation + ' of ' + $scope.get_measure_name(k, $scope.datasetdata[k].metrics[d].metricval, 'unique', 'label'));
							}else{
								temp_string = temp_string.split(exp7).join($scope.datasetdata[k].metrics[d].operation + ' of ' + $scope.get_measure_name(k, $scope.datasetdata[k].metrics[d].metricval, 'measure', 'label'));
							}
                             temp_string = temp_string.split(exp701).join($scope.datasetdata[k].metrics[d].operation);
                             temp_string = temp_string.split(exp801).join($scope.datasetdata[k].metrics[d].operation + '_' + $scope.datasetdata[k].metrics[d].metricval);
                             temp_string = temp_string.split(exp8).join($scope.datasetdata[k].metrics[d].operation + "('" + $scope.datasetdata[k].metrics[d].metricval + "')");
                             temp_string = temp_string.split(exp9).join($scope.datasetdata[k].metrics[d].metricval);
                             temp_string = temp_string.split(exp901).join($scope.datasetdata[k].metrics[d].metricval);
                             temp_string = temp_string.split(exp10).join('[\\"' + $scope.datasetdata[k].metrics[d].operation + '\\",' + '\\"' + $scope.datasetdata[k].metrics[d].metricval + '\\"]');
                         } else {
                             let tstr = "count()";
                             let tstr2 = "count";
                             temp_string = temp_string.split(exp7).join('# of Rows');
                             temp_string = temp_string.split(exp701).join('count');
                             temp_string = temp_string.split(exp8).join(tstr);
                             temp_string = temp_string.split(exp801).join(tstr2);
                             temp_string = temp_string.split(exp9).join('');
                             temp_string = temp_string.split(exp901).join('*');
                             temp_string = temp_string.split(exp10).join('[\\"count\\",\\"*\\"]');
                         }
                     }
                 }
                 t_obj.strings = $scope.datasetdata[k].strings;
                 for (var d = 0; d < $scope.datasetdata[k].strings.length; d++) {
                     let tempd = d + 1;
                     let expstr = '#' + curnt_cnt + 'String' + tempd + '#';
                     temp_string = temp_string.split(expstr).join($scope.datasetdata[k].strings[d]);
                 }
                 let exp11 = '#' + curnt_cnt + 'FilterLabel1#';
                 let exp12 = '#' + curnt_cnt + 'FilterLabel2#';
                 let exp13 = '#' + curnt_cnt + 'FilterLabel3#';
                 let exp14 = '#' + curnt_cnt + 'FilterLabel4#';
                 let exp15 = '#' + curnt_cnt + 'FilterLabel5#';
                 let exp16 = '#' + curnt_cnt + 'Filter1#';
                 let exp17 = '#' + curnt_cnt + 'Filter2#';
                 let exp18 = '#' + curnt_cnt + 'Filter3#';
                 let exp19 = '#' + curnt_cnt + 'Filter4#';
                 let exp20 = '#' + curnt_cnt + 'Filter5#';
                 let exp21 = '}",   ]';
                 let exp22 = '}",     ]';
                 let exp23 = '}",  ]';
                 let exp24 = '}", ]';
                 let exp25 = '}",]';
                 let exp26 = '"step": "Raw_Data"';
                 let exp27 = 'count(\'\')';
                 //let tm = $scope.datasetdata[k].selecteddeffilter+' in all';
                 let tm = ' in all';
                 temp_string = temp_string.split(exp11).join('All');
                 temp_string = temp_string.split(exp12).join('');
                 temp_string = temp_string.split(exp13).join('');
                 temp_string = temp_string.split(exp14).join('');
                 temp_string = temp_string.split(exp15).join('');
                 temp_string = temp_string.split(exp16).join(tm);
                 temp_string = temp_string.split(exp17).join('');
                 temp_string = temp_string.split(exp18).join('');
                 temp_string = temp_string.split(exp19).join('');
                 temp_string = temp_string.split(exp20).join('');
                 temp_string = temp_string.split(exp21).join('}"]');
                 temp_string = temp_string.split(exp22).join('}"]');
                 temp_string = temp_string.split(exp23).join('}"]')
                 temp_string = temp_string.split(exp24).join('}"]')
                 temp_string = temp_string.split(exp25).join('}"]');
                 temp_string = temp_string.split(exp26).join('"step": "Raw_Data"');
                 temp_string = temp_string.split(exp27).join('count()');
                 //temp_string = temp_string.replace(/\s/g, "")
                 console.log(temp_string);
                 $scope.saveobj.dsdata.push(t_obj);
                 if (k == $scope.datasetdata.length - 1) {
                     cb(temp_string);
                 }
             }
         }
         $scope.savedatatoorg = function () {
             $scope.saveobj.dsdata = JSON.stringify($scope.saveobj.dsdata);
             $scope.saveobj.template = $scope.crnttemp;
             $scope.saveobj.contact = $scope.org_user.Id;
             $scope.saveobj.dashboardname = $scope.newdashboardname;
             $scope.saveobj.access_token = localStorage.getItem('access_token')
             $scope.saveobj.instance_url = localStorage.getItem('instanceUrl')
             if ($scope.saveobj) {
                 $http({
                     url: '/dashboard/Savecopy',
                     method: "POST",
                     data: JSON.stringify($scope.saveobj)
                 }).then(function successCallback(response) {
                     console.log(response)
                     $scope.loading = false;
                     $.notify("Dashboard created succesfully", "success");
                     console.log(response);
                 }, function errorCallback(response) {
                     $scope.loading = false;
                     console.log(response);
                     $.notify("Error creating Dashboard " + response.data[0].message, "warn");
                     //localStorage.clear();
                     //window.location.href = "https://"+window.location.hostname;
                 });
             }
         }
         $scope.replaceStaticString = function () {
             if ($scope.dashboarddata && $scope.datasetdata.length > 0) {
                 $scope.manipulatestring(function (temp_string) {
                     if (localStorage.getItem('cust_access_token') && localStorage.getItem('cust_instanceUrl')) {
                         var create_url = localStorage.getItem('cust_instanceUrl') + '/services/data/' + $scope.dashboarddata.API_Version__c + '/wave/dashboards';
                         send_obj = {};
                         send_obj.access_token = localStorage.getItem('cust_access_token');
                         send_obj.url = create_url;
                         send_obj.data = temp_string;
                         $http({
                             url: '/dashboard/createdashboard',
                             method: "POST",
                             data: JSON.stringify(send_obj)
                         }).then(function successCallback(response) {
                             console.log(response);
                             $scope.savedatatoorg();
                         }, function errorCallback(response) {
                             $scope.loading = false;
                             console.log(response);
                             $.notify("Error creating Dashboard " + response.data[0].message, "warn");
                             //localStorage.clear();
                             //window.location.href = "https://"+window.location.hostname;
                         });
                         /*$http({
                             url: create_url,
                             method: "POST",
                             headers: {
                                 'Authorization': 'Bearer '+localStorage.getItem('cust_access_token'),
                                 'content-type': 'application/json'
                             },
                             data: temp_string
                         }).then(function successCallback(response) {
                                 $scope.loading = false;
                                 $.notify("Dashboard created succesfully", "success");
                                 console.log(response);
                             }, function errorCallback(response) {
                                 $scope.loading = false;
                                 console.log(response);
                                 $.notify("Error creating Dashboard "+ response.data[0].message, "warn");
                                 //localStorage.clear();
                                 //window.location.href = "https://"+window.location.hostname;
                         });*/
                     }
                 });
             }

         }
         var paramlist = getParams(window.location.href);
         console.log(localStorage.getItem('cust_access_token'));
         console.log(localStorage.getItem('cust_instanceUrl'));
         if (!localStorage.getItem('cust_access_token') || !localStorage.getItem('cust_instanceUrl')) {
             window.location.href = "https://" + window.location.hostname;
         }
         $scope.dashboardsec = true;
         $scope.datasetsec = false;
         $scope.metricsec = false;
         $scope.init();
         $scope.loading = true;
     }]);